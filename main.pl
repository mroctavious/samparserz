#!/usr/bin/perl

use 5.10.0;
use strict;
use threads;
use threads::shared;
  
use warnings;

##Pasos para la actualizacion
##Tabla de hash bi dimencional
#my %samResult{}{}

##Aqui usamos la API de samParser:
use File::Basename;
use lib dirname (__FILE__);
use samParser;


##Capturamos los argumentos
my $aux_pos=1;
my @samFiles;
my $refGenFile="";
my $outputDir="";

##Va contando cuantas subllaves tiene esa posicion
my $pos;
my $CHRM_COLUMN=2;
my $POS_COLUMN=3;
my $CIGAR_COLUMN=5;
my $SEQUENCE_COLUMN=9;

##Possible options:
##Get deletions
my %options;
$options{"deletion"}=0;
$options{"insertion"}=0;
$options{"match"}=0;
$options{"mismatch"}=0;
$options{"indel"}=0;
$options{"splitread"}=0;
#my %hashMaxSizes;
#$hashMaxSizes{"D"}=0;
#$hashMaxSizes{"X"}=0;
#$hashMaxSizes{"="}=0;
#$hashMaxSizes{"I"}=0;
#$hashMaxSizes{"IND"}=0;
#$hashMaxSizes{"SR"}=0;
my $totalReads=0;
sub say
{
	if( $_ )
	{
		print ("$_\n");
	}
}
sub errors
{
	my $errorNum=$_[0];
	my $string=$_[1];
	if( $errorNum == 1 )
	{
		print "Wrong Syntax!";
		print "./main -rg referenceGenome.fa -o outFolder file1.sam file2.sam\n";
		print "./main SAMs/D7.sam mm10New.fa \n";
		print "Options:\n";
		print "\t-d\tDeletions\n";
		print "\t-s\tSplitreads\n";
		print "\t-i\tInsertions\n";
		print "\t-ind\tIndels\n";
		print "\t-m\tMatches\n";
		print "\t-x\tMismatches\n";
		print "Another example can be:\n";
		print "\t./main.pl file1.sam file2.sam -d -i -s -rg referenceGenome.fa -o outFolder\n";
		print "The above example will only look for deletions, insetions and splitread.\n\n";
		exit 1;
	}
	elsif( $errorNum == 2)
	{	
		die "Unable to create output folder $string\nFolder already exists?";
	}
}

sub loadFiles
{
	my %hash;
	my @files=@_;
	my $j=@files;
	my $key="";
	for(my $i=0; $i<$j; $i++ )
	{
		open(my $openedFile, '<', "$files[$i]");
		my $file = basename($files[$i]);
		my @line;
		while(<$openedFile>)
		{
			chomp;
			#0=Starting pos
			#1=Type
			#2=Size
			#3=Sequence
			#4=TimesRepeated
			@line=split /\,/, $_;
			$key="$line[0]|$line[3]";
			if( exists( $hash{$key} ) )
			{
				$hash{$key}="$hash{$key};$file:$line[4]";			
			}
			else
			{
				$hash{$key}="$file:$line[4]";			
			}
			
		}
	}
	return \%hash;
}


sub createTables
{
	my ($hash, $out, @files)=@_;
	my %fileNames;
	my $i=0;
	my @archivos;

	open(my $filePtr, '>', "$out");
	##Mapeamos los archios al arreglo
	foreach(@files)
	{
		my $file = basename($_);
		$file="$file$i";
		$fileNames{$file}=$i;
		$i++;
	}
	print $filePtr "#Position|Sequence";
	foreach my $baseName( keys(%fileNames) )
	{
		print $filePtr "\t$baseName";
	}
	print $filePtr "\n";

	##For each unique sequence, see in which files they are.
	foreach my $posSeq( (keys %{$hash}) )
	{
		my @filesFound=split /\;/, $hash->{$posSeq};
		@archivos = (0) x $i;
		$i=0;
		foreach(@filesFound)
		{
			my ($fileName, $times)=split /\:/, $_;
			$fileName=basename($fileName);
			$archivos[$fileNames{"$fileName$i"}]=$times;
			$i++;
		}
		print $filePtr "$posSeq";
		foreach my $name ( keys(%fileNames) )
		{
			print $filePtr "\t$archivos[$fileNames{$name}]";
		}
		print $filePtr "\n";
	}

	close($filePtr);
	#foreach my $key ( keys %{$hash} )
	#{
	#	say "Key: $key    Value:$hash->{$key}";
	#}
	
	##Ponemos en 0 todas las posiciones del archivo
}


sub createDir
{
	my $directory = $_[0];
	if( -d "$directory" )
	{
		print "Using existing directory: $directory\n";
	}
	else
	{
		unless(mkdir($directory, 0755))
		{
			errors(2,$directory);
		}

	}
}


sub analyzeOptions
{
	my $args=shift;
	my %opts=%$args;
	foreach my $key (keys %opts)
	{
		print "Key:$key->$opts{$key}\n";
	}
	foreach my $key (keys %opts)
	{
		if( $opts{$key} == 1 )
		{
			return  0;
		}
	}
	return 1;
}

sub getOptions
{
	my @entry=@_;
	my $outFlag=0;
	my $rgFlag=0;
	foreach (@entry)
	{
		my @chars = split //, $_;
		if( $chars[0] eq "-" )
		{
			if( $_ eq "-d" )
			{
				$options{"deletion"}=1;
				next;
			}
			elsif( $_ eq "-i" )
			{
				$options{"insertion"}=1;
				next;
			}
			elsif( $_ eq "-m" )
			{
				$options{"match"}=1;
				next;
			}
			elsif( $_ eq "-x" )
			{
				$options{"mismatch"}=1;
				next;
			}
			elsif( $_ eq "-ind" )
			{
				$options{"indel"}=1;
				next;
			}
			elsif( $_ eq "-s" )
			{
				$options{"splitread"}=1;
				next;
			}
			elsif( $_ eq "-o" )
			{
				$outFlag=1;
			}
			elsif( $_ eq "-rg" )
			{
				$rgFlag=1;
			}
			else
			{
				errors(1);
			}

		}
		elsif( $outFlag == 1 )
		{
			$outputDir=$_;
			$outFlag=0;
		}
		elsif( $rgFlag == 1 )
		{
			$refGenFile=$_;
			$rgFlag=0;
		}
		else
		{
			#say "Empujando $_";
			push(@samFiles, $_);	
		}
	}
}

##Funcion que agrega valor a hash
sub addToHashM
{
	my $hash=$_[0];
	my $unsortedKey=$_[1];
	my $chromosome=$_[2];

	if( exists $hash->{"M"}{$chromosome}{$unsortedKey} )
	{
		$hash->{"M"}{$chromosome}{$unsortedKey}+=1;
	}
	else
	{
		$hash->{"M"}{$chromosome}{$unsortedKey}=1;
	}
	#if( $type eq "M" )
	#elsif( $type eq "X" )
	#elsif( $type eq "=" )
	#elsif( $type eq "I" )
	#elsif( $type eq "D" )
	#elsif( $type eq "IND" )
	#elsif( $type eq "SR" )

	return;
}


##Funcion que agrega valor a hash
sub addToHashX
{
	my $hash=$_[0];
	my $unsortedKey=$_[1];
	my $chromosome=$_[2];

	if( exists $hash->{"X"}{$chromosome}{$unsortedKey} )
	{
		$hash->{"X"}{$chromosome}{$unsortedKey}+=1;
	}
	else
	{
		$hash->{"X"}{$chromosome}{$unsortedKey}=1;
	}

	return;
}


##Funcion que agrega valor a hash
sub addToHashEq
{
	my $hash=$_[0];
	my $unsortedKey=$_[1];
	my $chromosome=$_[2];

	if( exists $hash->{"="}{$chromosome}{$unsortedKey} )
	{
		$hash->{"="}{$chromosome}{$unsortedKey}+=1;
	}
	else
	{
		$hash->{"="}{$chromosome}{$unsortedKey}=1;
	}

	return;
}


##Funcion que agrega valor a hash
sub addToHashI
 {
	my $hash=$_[0];
	my $unsortedKey=$_[1];
	my $chromosome=$_[2];

	if( exists $hash->{"I"}{$chromosome}{$unsortedKey} )
	{
		$hash->{"I"}{$chromosome}{$unsortedKey}+=1;
	}
	else
	{
		$hash->{"I"}{$chromosome}{$unsortedKey}=1;
	}

	return;
}


##Funcion que agrega valor a hash
sub addToHashD
{
	my $hash=$_[0];
	my $unsortedKey=$_[1];
	my $chromosome=$_[2];

	if( exists $hash->{"D"}{$chromosome}{$unsortedKey} )
	{
		$hash->{"D"}{$chromosome}{$unsortedKey}+=1;
	}
	else
	{
		$hash->{"D"}{$chromosome}{$unsortedKey}=1;
	}

	return;
}


##Funcion que agrega valor a hash
sub addToHashSR
{
	my $hash=$_[0];
	my $unsortedKey=$_[1];
	my $chromosome=$_[2];

	if( exists $hash->{"SR"}{$chromosome}{$unsortedKey} )
	{
		$hash->{"SR"}{$chromosome}{$unsortedKey}+=1;
	}
	else
	{
		$hash->{"SR"}{$chromosome}{$unsortedKey}=1;
	}

	return;
}


##Funcion que agrega valor a hash
sub addToHashIND
{
	my $hash=$_[0];
	my $unsortedKey=$_[1];
	my $chromosome=$_[2];

	if( exists $hash->{"IND"}{$chromosome}{$unsortedKey} )
	{
		$hash->{"IND"}{$chromosome}{$unsortedKey}+=1;
	}
	else
	{
		$hash->{"IND"}{$chromosome}{$unsortedKey}=1;
	}

	return;
}

sub cigarAnalizer
{
	my $position=$_[0];
	my $cigarString=$_[1];
	my $sequence=$_[2];
	my $referenceGenome=$_[3];
	my $GENOME_LENGTH=$_[4];
	my $chromosome=$_[5];
	my $hash=$_[6];
	
	##Spliteamos el cigar
	my @cigar = split( /(?<=\d)(?=\D)|(?<=\D)(?=\d)/, $cigarString );
			
	##El tamaño total del cigar
	my $cigarSize=@cigar;

	##Se guarda la posicion inicial
	my $currentPos=$position;
	
	##Se guarda la posicion inicial
	my $initPos=$position;
	
	my $substring="";
	my $sequencePosition=0;
	
	##Bandera en caso de encontrar un INDEL
	my $flag=0;
	
	##Se recorre todo el CIGAR y se analiza
	for(my $i=0; $i<$cigarSize; $i+=2)
	{
	
		##Evita que el cigar sea analisado en el caso de los INDELs por ejemplo
		if($flag==1)
		{
			##Desactivar bandera
			$flag=0;
			next;
		}
		
		##Activator verificara si se vuelve a regresar a la primera posicion del genoma
		my $activator=0;


		##Si se llega a pasar de la longitud del genoma de referencia
		if( $cigar[$i]+$currentPos > $GENOME_LENGTH)
		{
			$activator=1;
		}
		
		##En caso de que sea un Match, MisMatch
		if( $cigar[$i+1] eq "M" || $cigar[$i+1] eq "X" || $cigar[$i+1] eq "=")
		{
			
			##Se abstrae la secuencia y se guarda como una llave, para poder manipularla despues
			my $ResultToSaveInHashMM=samParser::cigarMatchMismatch($currentPos, $sequencePosition, $cigar[$i+1],$sequence, $cigar[$i]);
			
			##Toma en cuenta la posicion e imprime donde encontro el Match o Mismatch
			$currentPos+=$cigar[$i];

			##Se suma a la posicion de la secuencia
			$sequencePosition+=$cigar[$i];
			
			###Aqui manipular $ResultToSaveInHash o mandar a guardar
			#say "$chromosome | $ResultToSaveInHashMM";
			if( $cigar[$i+1] eq "M" )
			{
				addToHashM($hash, $ResultToSaveInHashMM, $chromosome);
			}
			elsif( $cigar[$i+1] eq "=" )
			{
				addToHashEq($hash, $ResultToSaveInHashMM, $chromosome);
			}
			else
			{
				addToHashX($hash, $ResultToSaveInHashMM, $chromosome);
			}
		}
		
			
		##Insercion
		elsif( $cigar[$i+1] eq "I")
		{
			##Calcula la secuencia de la insercion
			my $ResultToSaveInHashIns=samParser::cigarInsertion( $currentPos, $sequencePosition, $cigar[$i+1], $sequence, $cigar[$i] );
			my $result="";
			my $wasIndel=0;
			##Empieza analizar si es candidato a un INDEL
			if ( samParser::isIndel($cigarSize, $i, @cigar) )
			{
				##Conseguimos la secuencia de la insercion para mandarla a la funcion de cigarIndel
				my @insertionSplt=split /\|/, $ResultToSaveInHashIns;
				
				##Limpiamos de espacios y saltos de linea
				chomp @insertionSplt;
				
				my $insertionSequence=$insertionSplt[2];

				##Calcular INDEL
				#my $ResultToSaveInHashInD=samParser::cigarIndel($referenceGenome, $insertionSequence, $currentPos, $initPos, $cigar[$i+2], $GENOME_LENGTH, $i, $activator, @cigar);
				my @ResultToSaveInHashSR=samParser::deletionSubstringIndex($referenceGenome, $currentPos, $cigar[$i+2], $GENOME_LENGTH, $activator);
				$ResultToSaveInHashSR[0]=$ResultToSaveInHashSR[0]+$cigar[$i];
				my @insertSeq=split /\|/, $ResultToSaveInHashIns;
				my $size=$cigar[$i]+$cigar[$i+2];
				my $key="$insertSeq[0]"."ND|$size|$insertSeq[2]+$chromosome:$ResultToSaveInHashSR[0]-$ResultToSaveInHashSR[1]";
				#say $key;
				##Si cumplio con todas las anclas, entonces es un INDEL y modifica la posicion ya que este cambio
				if(@ResultToSaveInHashSR)
				{
					$currentPos+=$cigar[$i];
					$result=$key;
					$flag=1;

				}
				else
				{
					$result=$ResultToSaveInHashIns;

				}
			}
			else
			{
				$result=$ResultToSaveInHashIns;
			}
						
			##$result es el resultado de todo el analisis del INDEL
			if( $flag == 1 )
			{
				addToHashIND($hash, $result, $chromosome);	
			}
			else
			{
				addToHashI($hash, $result, $chromosome);
			}
				##Como fue insercion, se aumenta el contador de la secuencia del genoma de ref
				$sequencePosition+=$cigar[$i];
		}
		
		##Delecion
		elsif($cigar[$i+1] eq "D")
		{			
			#say "Entre al if";
			my $ResultToSaveInHashDel=samParser::cigarDeletion($referenceGenome, $currentPos, $cigar[$i], $GENOME_LENGTH, $activator);
		
			$currentPos+=$cigar[$i];
			#say "$chromosome | $ResultToSaveInHashDel";
			addToHashD($hash, $ResultToSaveInHashDel, $chromosome);

		}
		
		##Splitread
		elsif($cigar[$i+1] eq "N")
		{
			#my $ResultToSaveInHashSR=samParser::cigarSplitread($referenceGenome, $currentPos, $initPos, $cigar[$i], $i, $GENOME_LENGTH, $activator, @cigar);			
			my @ResultToSaveInHashSR=samParser::deletionSubstringIndex($referenceGenome, $currentPos, $cigar[$i], $GENOME_LENGTH, $activator);
			my $key="$ResultToSaveInHashSR[0]"."SR|$cigar[$i]|$chromosome:$ResultToSaveInHashSR[0]-$ResultToSaveInHashSR[1]";

			if(@ResultToSaveInHashSR)
			{
				#say "$chromosome | $ResultToSaveInHashSR";
				addToHashSR($hash, $key, $chromosome);
			}
			$currentPos+=$cigar[$i];
		}
	
		else
		{
			##Si toma en cuenta su posicion
			$currentPos+=$cigar[$i]-1;

			##Se suma a la posicion de la secuencia
			$sequencePosition+=$cigar[$i];			
		}
		
		if($activator==1)
		{
			$currentPos=1;	
		}
	}
}


sub cigarAnalizerNotDefault
{
	my $position=$_[0];
	my $cigarString=$_[1];
	my $sequence=$_[2];
	my $referenceGenome=$_[3];
	my $GENOME_LENGTH=$_[4];
	my $chromosome=$_[5];
	my $hash=$_[6];
	
	##Spliteamos el cigar
	my @cigar = split( /(?<=\d)(?=\D)|(?<=\D)(?=\d)/, $cigarString );
			
	##El tamaño total del cigar
	my $cigarSize=@cigar;

	##Se guarda la posicion inicial
	my $currentPos=$position;
	
	##Se guarda la posicion inicial
	my $initPos=$position;
	
	my $substring="";
	my $sequencePosition=0;
	
	##Bandera en caso de encontrar un INDEL
	my $flag=0;
	
	##Se recorre todo el CIGAR y se analiza
	for(my $i=0; $i<$cigarSize; $i+=2)
	{
	
		##Evita que el cigar sea analisado en el caso de los INDELs por ejemplo
		if($flag==1)
		{
			##Desactivar bandera
			$flag=0;
			next;
		}
		
		##Activator verificara si se vuelve a regresar a la primera posicion del genoma
		my $activator=0;


		##Si se llega a pasar de la longitud del genoma de referencia
		if( $cigar[$i]+$currentPos > $GENOME_LENGTH)
		{
			$activator=1;
		}
		
		##En caso de que sea un Match, MisMatch
		if( $cigar[$i+1] eq "M" || $cigar[$i+1] eq "=")
		{
			if( $options{"match"} == 1  )
			{
				##Se abstrae la secuencia y se guarda como una llave, para poder manipularla despues
				my $ResultToSaveInHashMM=samParser::cigarMatchMismatch($currentPos, $sequencePosition, $cigar[$i+1],$sequence, $cigar[$i]);

				##Toma en cuenta la posicion e imprime donde encontro el Match o Mismatch
				$currentPos+=$cigar[$i];

				##Se suma a la posicion de la secuencia
				$sequencePosition+=$cigar[$i];

				###Aqui manipular $ResultToSaveInHash o mandar a guardar
				addToHashM($hash, $ResultToSaveInHashMM, $chromosome);
			}
			else
			{
				##Toma en cuenta la posicion e imprime donde encontro el Match o Mismatch
				$currentPos+=$cigar[$i];

				##Se suma a la posicion de la secuencia
				$sequencePosition+=$cigar[$i];

			}
		}
		##En caso de que sea un Match, MisMatch
		elsif( $cigar[$i+1] eq "X" )
		{
			if( $options{"mismatch"} == 1 )
			{
				##Se abstrae la secuencia y se guarda como una llave, para poder manipularla despues
				my $ResultToSaveInHashMM=samParser::cigarMatchMismatch($currentPos, $sequencePosition, $cigar[$i+1],$sequence, $cigar[$i]);

				##Toma en cuenta la posicion e imprime donde encontro el Match o Mismatch
				$currentPos+=$cigar[$i];

				##Se suma a la posicion de la secuencia
				$sequencePosition+=$cigar[$i];

				###Aqui manipular $ResultToSaveInHash o mandar a guardar
				#say "$chromosome | $ResultToSaveInHashMM";
				addToHashX($hash, $ResultToSaveInHashMM, $chromosome);
			}
			else
			{
				##Toma en cuenta la posicion e imprime donde encontro el Match o Mismatch
				$currentPos+=$cigar[$i];

				##Se suma a la posicion de la secuencia
				$sequencePosition+=$cigar[$i];

			}
		}
			
		##Insercion
		elsif( $cigar[$i+1] eq "I" )
		{
			if( $options{"insertion"} == 1 || $options{"indel"} == 1 )
			{
				##Calcula la secuencia de la insercion
				my $ResultToSaveInHashIns=samParser::cigarInsertion( $currentPos, $sequencePosition, $cigar[$i+1], $sequence, $cigar[$i] );
				my $result="";
				my $wasIndel=0;
				##Empieza analizar si es candidato a un INDEL
				if ( samParser::isIndel($cigarSize, $i, @cigar) && $options{"indel"} == 1 )
				{
					##Conseguimos la secuencia de la insercion para mandarla a la funcion de cigarIndel
					my @insertionSplt=split /\|/, $ResultToSaveInHashIns;
				
					##Limpiamos de espacios y saltos de linea
					chomp @insertionSplt;
				
					my $insertionSequence=$insertionSplt[2];
				
					##Calcular INDEL
					my $ResultToSaveInHashInD=samParser::cigarIndel($referenceGenome, $insertionSequence, $currentPos, $initPos, $cigar[$i+2], $GENOME_LENGTH, $i, $activator, @cigar);
					##Si cumplio con todas las anclas, entonces es un INDEL y modifica la posicion ya que este cambio
					if($ResultToSaveInHashInD)
					{
						$currentPos+=$cigar[$i];
						$result=$ResultToSaveInHashInD;
						$flag=1;
					}
					else
					{
						$result=$ResultToSaveInHashIns;
					}
				}
				else
				{
					$result=$ResultToSaveInHashIns;
				}
			
				##Como fue insercion, se aumenta el contador de la secuencia del genoma de ref
				$sequencePosition+=$cigar[$i];
			
				##$result es el resultado de todo el analisis del INDEL
				#say "$chromosome | $result";
				if( $flag == 1 )
				{
					addToHashIND($hash, $result, $chromosome);	
				}
				elsif( $options{"insertion"} == 1 )
				{
					addToHashI($hash, $result, $chromosome);
				}
			}
			else
			{
				##Como fue insercion, se aumenta el contador de la secuencia del genoma de ref
				$sequencePosition+=$cigar[$i];
			}
		}
		
		##Delecion
		elsif($cigar[$i+1] eq "D")
		{			
			#say "Entre al if";
			if( $options{"deletion"} == 1)
			{
				my $ResultToSaveInHashDel=samParser::cigarDeletion($referenceGenome, $currentPos, $cigar[$i], $GENOME_LENGTH, $activator);
		
				$currentPos+=$cigar[$i];
				#say "$chromosome | $ResultToSaveInHashDel";
				addToHashD($hash, $ResultToSaveInHashDel, $chromosome);
			}
			else
			{
				$currentPos+=$cigar[$i];
			}
		}
		
		##Splitread
		elsif($cigar[$i+1] eq "N")
		{
			if($options{"splitread"} == 1)
			{
				my @ResultToSaveInHashSR=samParser::deletionSubstringIndex($referenceGenome, $currentPos, $cigar[$i], $GENOME_LENGTH, $activator);
				my $key="$ResultToSaveInHashSR[0]"."SR|$cigar[$i]|$chromosome:$ResultToSaveInHashSR[0]-$ResultToSaveInHashSR[1]";
				#my $ResultToSaveInHashSR=samParser::cigarSplitread($referenceGenome, $currentPos, $initPos, $cigar[$i], $i, $GENOME_LENGTH, $activator, @cigar);			
				if(@ResultToSaveInHashSR)
				{
					#say "$chromosome | $ResultToSaveInHashSR";
					addToHashSR($hash, $key, $chromosome);
				}
				$currentPos+=$cigar[$i];
			}
			else
			{
				$currentPos+=$cigar[$i];
			}
		}
	
		else
		{
			##Si toma en cuenta su posicion
			$currentPos+=$cigar[$i]-1;

			##Se suma a la posicion de la secuencia
			$sequencePosition+=$cigar[$i];			
		}
		
		if($activator==1)
		{
			$currentPos=1;	
		}
		$totalReads+=1;
	}
}

sub openSamFile
{
	my %hash;
	open my $SAM_FILE, '<', $_[0];
	my $ptr_hash_Genome=$_[1];
	my $ptr_hash_Lengths=$_[2];
	#my $referenceGenome=samParser::readReferenceGenome($refGenFile);
	
	##Recorrer linea por linea
	while(<$SAM_FILE>)
	{
		chomp;
	
		##Si es parte de un header, entonces continuar con la siguiente linea
		if ($_ =~ /^\s*@/)
		{
			next;
		}
		
		##Separo por [TABS]
		my @read = split "\t", $_;
		
		#Consigo posicion en el genoma de referencia, dicho por el read
		my $position = $read[$POS_COLUMN];
		
		##Conseguimos el cigar del read
		my $cigarString = $read[$CIGAR_COLUMN];
		
		##Conseguir la secuencia que nos dio el read
		my $readSequence = $read[$SEQUENCE_COLUMN];

		my $readID = $read[0];
		my $readFlag = $read[1];
		
		##Conseguir chromosoma
		my $chromosome=$read[$CHRM_COLUMN];
		
		##Separo por letras y numeros y hace el CIGAR. La posicion par es el numero y la posicion impar es la letra
		if( $cigarString !~ /[*]/)
		{
			##Get the sequence from desired chromosome
			my $ptr_sequence=samParser::getChromosome($chromosome, $ptr_hash_Genome);
			my $genomeLength=samParser::getChromosomeLength($chromosome, $ptr_hash_Lengths);
			#say "\@ID:$readID";
			#say "\@FLAG:$readFlag";
			cigarAnalizer($position, $cigarString, $readSequence, $ptr_sequence, $genomeLength, $chromosome, \%hash, $chromosome );
			$totalReads+=1;
		}
	}
	return \%hash;
}

sub openSamFileNotDefault
{
	my %hash;
	open my $SAM_FILE, '<', $_[0];
	my $ptr_hash_Genome=$_[1];
	my $ptr_hash_Lengths=$_[2];
	#my $referenceGenome=samParser::readReferenceGenome($refGenFile);
	my $ptr_sequence;
	my $genomeLength;
	##Recorrer linea por linea
	while(<$SAM_FILE>)
	{
		chomp;
	
		##Si es parte de un header, entonces continuar con la siguiente linea
		if ($_ =~ /^\s*@/)
		{
			next;
		}
		
		##Separo por [TABS]
		my @read = split "\t", $_;
		
		#Consigo posicion en el genoma de referencia, dicho por el read
		my $position = $read[$POS_COLUMN];
		
		##Conseguimos el cigar del read
		my $cigarString = $read[$CIGAR_COLUMN];
		
		##Conseguir la secuencia que nos dio el read
		my $readSequence = $read[$SEQUENCE_COLUMN];

		my $readID = $read[0];

		my $readFlag = $read[1];
		
		##Conseguir chromosoma
		my $chromosome=$read[$CHRM_COLUMN];
		
		##Separo por letras y numeros y hace el CIGAR. La posicion par es el numero y la posicion impar es la letra
		if( $cigarString !~ /[*]/)
		{
			##Get the sequence from desired chromosome
			$ptr_sequence=samParser::getChromosome($chromosome, $ptr_hash_Genome);
			$genomeLength=samParser::getChromosomeLength($chromosome, $ptr_hash_Lengths);
			#say "\@ID:$readID";
			#say "\@FLAG:$readFlag";
			cigarAnalizerNotDefault($position, $cigarString, $readSequence, $ptr_sequence, $genomeLength, $chromosome, \%hash, $chromosome );
		}
	}
	return \%hash;
}

sub startParsing
{

	##Get the options the user gave
	getOptions(@_);
	my $default=analyzeOptions(\%options);

	if ( ! @samFiles || ! $refGenFile  || ! $outputDir )
	{
		errors(1);
	}

	##Intentamos crear folder de salida
	createDir($outputDir);

	##Abrimos el archivo
	open my $RG_FILE, '<', $refGenFile;

	##Variable para calcular la posicion en el genoma de referencia
	my $i_last_POS;

	##Read reference genome
	my $ptr_hash_Genome=samParser::readReferenceGenome($refGenFile);

	##Get the lengths of the chromosomes
	my $hash_lengths=samParser::createChromosomesLengths($ptr_hash_Genome);
	my @results;
	my @threads;
	my $j=0;
	my @files;
	if($default == 1)
	{
		##Invocacion de hilos
		foreach (@samFiles)
		{
			my $var=$_;
			$files[$j]="$var";
			print("File: $files[$j]\n");
			#push @threads, (threads->create(\&openSamFile, $_, $ptr_hash_Genome, $hash_lengths));
			$results[$j]=openSamFile($_, $ptr_hash_Genome, $hash_lengths);
			$j+=1;
		}
	}
	else
	{
		##Invocacion de hilos
		foreach (@samFiles)
		{
			my $var=$_;
			$files[$j]="$var";
			print("File: $files[$j]\n");
			#push @threads, (threads->create(\&openSamFileNotDefault, $_, $ptr_hash_Genome, $hash_lengths));
			$results[$j]=openSamFileNotDefault($_, $ptr_hash_Genome, $hash_lengths);
			$j+=1;
		}

	}

	##Numero de hilos lanzados
	#my $thrdNum=@threads;

	#print "Se mandaron $thrdNum Hilos.\n";

	##Recorremos el arreglo de threads para guardar la referencia en el arreglo de hashes
	#for(my $i=0; $i<$thrdNum; $i++ )
	#{
	#	$results[$i]=($threads[$i]->join);
	#}

	#print(@files);

	my @allOutFiles;
	for(my $i=0; $i<$j; $i++ )
	{
		my $file = basename($files[$i]);
		open(my $fh, '>', "$outputDir/$file.smpy");
		#push("$outputDir/$file.smpy");
		print $fh "#Position,Type,Size,Sequence,TimesFound\n";
		print $fh "\@:$totalReads\n";
		my @tmpSubs;
		foreach my $type ( keys %{$results[$i]} )
		{
			foreach my $chromosomeKey ( keys %{ $results[$i]->{$type} } )
			{
				foreach my $seqKey ( keys %{ $results[$i]->{$type}{$chromosomeKey} } )
				{
					#print $fh "$seqKey,$results[$i]->{$type}{$chromosomeKey}{$seqKey}\n";
					@tmpSubs=split /\|/, $seqKey;
					my @l = split(/(?<=\d)(?=\D)|(?<=\D)(?=\d)/, $tmpSubs[0]);
					#print  "$seqKey,$results[$i]->{$type}{$chromosomeKey}{$seqKey}\n";
					print $fh "$l[0],$l[1],$tmpSubs[1],$tmpSubs[2],$results[$i]->{$type}{$chromosomeKey}{$seqKey},$chromosomeKey\n";
				}
			}
		}
		close $fh;
	}
	return @allOutFiles;
}

my @procFiles=startParsing(@ARGV);
foreach(@procFiles)
{
	print "$_ ";
}
print "\n";

