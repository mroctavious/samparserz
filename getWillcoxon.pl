use 5.010;
use File::Basename;
use lib dirname (__FILE__);
use samParser;
use Formulas;
my %hashSizes;
my %maxReadsPerFile;
my %hashAverage;
my %hashFormulaFactor;
use File::Basename;    
use Statistics::Test::WilcoxonRankSum;
use warnings;



sub errors
{
	my $errorNum=$_[0];
	my $string=$_[1];
	if( $errorNum == 1 )
	{
		print "Wrong Syntax!";
		print "./getWillcoxon.pl -rg referenceGenome.fa -o outFilename -vs 3 fileA_1.smpy fileA_2.smpy fileA_3.smpy fileB_1.smpy fileB_2.smpy\n";
		print "Options:\n";
		print "\t-rg [STRING]\tThe reference genome input file\n";
		print "\t-o [STRING]\tThe output file where the table will be written\n";
		print "\t-vs [INT]\t Number of Files of side A, This is for wilcoxon test\n";
		exit 1;
	}

}

my $outputName;
my $refGenFile;
my $vsValue;
my @smpyFiles;
sub getOptions
{
	my @entry=@_;
	my $outFlag=0;
	my $rgFlag=0;
	my $vsFlag=0;
	foreach (@entry)
	{
		my @chars = split //, $_;
		if( $chars[0] eq "-" )
		{
			if( $_ eq "-o" )
			{
				$outFlag=1;
			}
			elsif( $_ eq "-rg" )
			{
				$rgFlag=1;
			}
			elsif( $_ eq "-vs" )
			{
				$vsFlag=1;
			}
			else
			{
				errors(1);
			}

		}
		elsif( $outFlag == 1 )
		{
			$outputName=$_;
			$outFlag=0;
		}
		elsif( $rgFlag == 1 )
		{
			$refGenFile=$_;
			$rgFlag=0;
		}
		elsif( $vsFlag == 1 )
		{
			$vsValue=$_;
			$vsFlag=0;
		}
		else
		{
			#say "Empujando $_";
			push(@smpyFiles, $_);	
		}
	}
}




sub loadFiles
{
	my %hash;
	my ($ptr_hash_Lengths, @files)=@_;
	my $j=@files;
	my $key="";
	my $average=1;
	my @posFilenames;
	for(my $i=0; $i<$j; $i++ )
	{
		open(my $openedFile, '<', "$files[$i]");
		my $file = basename($files[$i]);
		if( $file =~ /R1/)
		{
			$hashAverage{$file}=245;
			$hashFormulaFactor{$file}=1.14285714;		

		}
		else
		{
			$hashAverage{$file}=180;
			$hashFormulaFactor{$file}=1.2;		
		}
		my @line;
		my $genomeLength=0;
		my $total=0;
		my @header;
		print("\tCargando archivo ".basename($files[$i], ".smpy") . "... ");
		$posFilenames[$i]=basename($files[$i], ".smpy");
		my $StartingPos;
		my $Type;
		my $Size;
		my $Sequence;
		my $TimesRepeated;
		my $Chromosome;

		my @values;

		while(<$openedFile>)
		{
			chomp;
			if ($_ =~ /^\s*\#/)
			{
				next;
			}
			if ($_ =~ /^\s*@/)
			{
				my @header=split /\:/, $_;
				$maxReadsPerFile{$file}=$header[1];
			}
			@line=split /\,/, $_;
			#0=Starting pos
			#1=Type
			#2=Size
			#3=Sequence
			#4=TimesRepeated
			#5=Chromosome
			$StartingPos=$line[0];
			$Type = $line[1];
			$Size = $line[2];
			$Sequence = $line[3];
			$TimesRepeated = $line[4];
			$Chromosome = $line[5];

			$genomeLength=samParser::getChromosomeLength( $Chromosome, $ptr_hash_Lengths);
			$total=0;

			$fResult=Formulas::nuevaFormula( $TimesRepeated, $hashAverage{$file}, $maxReadsPerFile{$file}, $hashFormulaFactor{$file});

			if( $fResult > 0 )
			{
				#say $fResult;
				@values=($Type, $Size, $Chromosome);
				if( exists( $hash{$StartingPos}{$Sequence}) )
				{
					push(@{$hash{$StartingPos}{$Sequence}}, ($i, $fResult));
				}
				else
				{
					push(@values, $i, $fResult);
					$hash{$StartingPos}{$Sequence} = [@values];
				}
			
			}
			
		}
		print("DONE\n");
	}
	return \%hash, @posFilenames;
}


sub anyGreaterThan0
{
	foreach ( @_ )
	{
		if( $_ > 0 )
		{
			return 1;		
		}
	}
	
	return 0;
}

sub createTables
{
	my ($hash, $out, $vs, @fileMapping)=@_;
	open( my $filePtr, '>', "$out");
	my %data=%{ $hash };
	say "Creando tabla... ";
	
	##Array values, is the value of the hash, array with the data and formula result
	#0=Type
	my $typePos=0;
	my $sizePos=1;
	my $chrPos=2;
	my @hashValue;
	my @fileFormulaResult;
	my $iterator=0;
	my $arraySize=0;
	my $strCreator="";
	my $numberOfFiles=@fileMapping;
	my @allFiles=(0) x $numberOfFiles;
	my @A;
	my @B;
	my $prob;
	my $wilcox_test;
	print $filePtr "PosicionInicial,Secuencia,Tamano,Tipo,Chr,".join(",", @fileMapping).",P-Value\n";
	foreach my $start( keys( %data ) )
	{
		foreach my $seq( keys( $data{$start} ) )
		{
			@allFiles=(0) x $numberOfFiles;
			@hashValue=@{$data{$start}{$seq}};
			$arraySize = @hashValue;
			for($iterator=3; $iterator < $arraySize; $iterator+=2 )
			{
				$allFiles[$hashValue[$iterator]]=$hashValue[$iterator+1];
			}

			##Para prueba del willcoxon
			@A=@allFiles[0..$vs-1];
			@B=@allFiles[$vs..$numberOfFiles-1];
			$strCreator=join(",", @allFiles);
			$prob=0;
			if( anyGreaterThan0( @A ) && anyGreaterThan0( @B ) )
			{
				##Iniciando prueba del wilcoxon
				$wilcox_test = Statistics::Test::WilcoxonRankSum->new();
				$wilcox_test->load_data(\@A, \@B);
				$prob = $wilcox_test->probability();
				print $filePtr "$start,$seq,$hashValue[$sizePos],$hashValue[$typePos],$hashValue[$chrPos],$strCreator,$prob\n";
			}
		}
	}
	
	close $filePtr;
}

my @args=@ARGV;
getOptions(@args);
if (! $outputName || ! $refGenFile || ! $vsValue || ! @smpyFiles )
{
	errors(1);
}

print "Cargando genoma de referencia... ";
##Read reference genome
my $ptr_hash_Genome=samParser::readReferenceGenome($refGenFile);
print "OK\n";



print "Calculando tamaños de cromosomas... ";
##Get the lengths of the chromosomes
my $hash_lengths=samParser::createChromosomesLengths($ptr_hash_Genome);
print "OK\n";

say ("Cargando archivos en memoria... ");
##Cargando archivos
my ($hash, @filesMapping)=loadFiles($hash_lengths, @smpyFiles);
print("OK\n\n");
my $tmpSize=@filesMapping;
my @tmpA=@filesMapping[0..$vsValue-1];
my @tmpB=@filesMapping[$vsValue..$tmpSize-1];
print( "The next comparison will be processed: ");
print join( ", ", @tmpA ). "  VS  ".join(", ", @tmpB);
print("\n\n");

createTables( $hash, $outputName, $vsValue, @filesMapping);



